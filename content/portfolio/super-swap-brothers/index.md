---
title: Super Swap Brothers
description: This is the description of our sample project
date: "2019-05-02T19:47:09+02:00"
jobDate: 2018
work: [games]
techs: [unity]
thumbnail: images/superswapbrother.png
projectUrl: https://squalex.itch.io/super-swap-brothers
---

Super Swap Brothers is a horizontal runner game. 

It's a 2 player game played on a single controller ! 
Jump above the obstacles and cooperate with your partner to swap at the same time to go through your corresponding door ! But beware, unpredictable events can happen...


This game has been developed during the HitBox Game Jam 2018 and won 2 prizes (audience and developer award) !
